package pl.codeleak.demos.sbt.home;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import pl.codeleak.demos.sbt.item.Item;
import pl.codeleak.demos.sbt.item.ItemStatusEnum;
import pl.codeleak.demos.sbt.itemCategory.ItemCategory;
import pl.codeleak.demos.sbt.order.OrderController;
import pl.codeleak.demos.sbt.order.OrderRepository;
import pl.codeleak.demos.sbt.order.OrderStateEnum;
import pl.codeleak.demos.sbt.order.Ordered;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
public class OrderControllerClassicTest {
	@Autowired
    private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Autowired
	OrderController orderControl;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Before
    public void initMocks(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }
	
	@After 
	public void reset_mocks() {
	    Mockito.reset(orderRepo);
	}
	
	@Test
	public void orderListTest() throws Exception {
		//Setup
		Ordered order = new Ordered();
    	order.setWorkerName("Henryk Lenryk");
    	order.setState(OrderStateEnum.OCZEKUJACE);
    	LocalDateTime now = LocalDateTime.now();
    	Date d2 = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
    	order.setDate(d2);

		when(orderRepo.findAll()).thenReturn(Arrays.asList(order));
		
		//Test
		mockMvc.perform(get("/order/list"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("orders", hasSize(1)));
		
		verify(orderRepo, times(1)).findAll();
		
	}
	
	@Test
	public void orderListTestNoOrders() throws Exception {
		//Setup

		when(orderRepo.findAll()).thenReturn(Arrays.asList());
		
		//Test
		mockMvc.perform(get("/order/list"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("orders", hasSize(0)));
		
		verify(orderRepo, times(1)).findAll();
		
	}
	
	@Test
	public void deleteTest() throws Exception {
		//Setup
		long id = 1;
		Ordered order = new Ordered();
		order.setId(id);
    	order.setWorkerName("Henryk Lenryk");
    	order.setState(OrderStateEnum.OCZEKUJACE);
    	LocalDateTime now = LocalDateTime.now();
    	Date d2 = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
    	order.setDate(d2);
    	
		when(orderRepo.findById(id)).thenReturn(order);
		
		//Test
		mockMvc.perform(delete("/order/" + id))
		.andDo(print())
			.andExpect(status().is3xxRedirection());
		
		verify(orderRepo, times(1)).delete(id);
		
	}
	
	@Test
	public void deleteTestWrongId() throws Exception {
		//Setup
		long id = 1;
    	
		when(orderRepo.findById(id)).thenReturn(null);
		
		//Test
		mockMvc.perform(delete("/order/" + id))
		.andDo(print())
			.andExpect(status().is3xxRedirection());
		
		verify(orderRepo, times(0)).delete(id);
		
		
	}
	
	
}
