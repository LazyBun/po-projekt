package pl.codeleak.demos.sbt.home;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.Assert;
import pl.codeleak.demos.sbt.Application;
import pl.codeleak.demos.sbt.item.Item;
import pl.codeleak.demos.sbt.item.ItemRepository;
import pl.codeleak.demos.sbt.item.ItemStatusEnum;
import pl.codeleak.demos.sbt.itemCategory.ItemCategory;
import pl.codeleak.demos.sbt.itemCategory.ItemCategoryRepository;
import pl.codeleak.selenium.support.SeleniumTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebIntegrationTest(value = "server.port=9000")
@SeleniumTest(driver = ChromeDriver.class, baseUrl = "http://localhost:9000")
public class HomeControllerTest {

	
    @Autowired
    private WebDriver driver;
    
    @Autowired
    private ItemRepository itemRepo;
    
    @Autowired
    private ItemCategoryRepository itemCatRepo;

    //private HomePage homePage;
    
    
    @Before
    public void setUp() throws Exception {
    	System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Google/Chrome/Application");
    	driver.manage().window().maximize();
    }
    
    
    private void seleniumWait() {
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Test
    public void T001_doesItemAddWork() {
    	//Setup
    	itemRepo.deleteAll();
    	//
    	
    	driver.navigate().to("http://localhost:9000/item/list");
    	int sizeBefore = driver.findElements(By.className("test-row")).size();
    	driver.findElement(By.id("test-add")).click();
    	Assert.assertEquals("Is in add?", "http://localhost:9000/item/add", driver.getCurrentUrl());
    	seleniumWait();
    	driver.findElement(By.id("name")).sendKeys("TestBardzo");
    	seleniumWait();
    	driver.findElement(By.id("description")).sendKeys("TestBardzoTestBardzoTestBardzoTestBardzoTestBardzoTestBardzo");
    	seleniumWait();
    	driver.findElement(By.id("price")).clear();;
    	driver.findElement(By.id("price")).sendKeys("18.99");
    	seleniumWait();
    	driver.findElement(By.id("test-add")).click();
    	Assert.assertEquals("Is in list?", "http://localhost:9000/item/list", driver.getCurrentUrl());
    	int sizeAfter = driver.findElements(By.className("test-row")).size();
    	Assert.assertEquals("Is size larger?", sizeBefore+1, sizeAfter);
    	seleniumWait();
    }
    
    @Test
    public void T002_doesItemEditWork() {
    	//Setup
    	itemRepo.deleteAll();
    	
    	ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
    	itemCatRepo.save(itemCat);
    	
    	Item item = new Item();
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	item = itemRepo.save(item);
    	//
    	
    	driver.navigate().to("http://localhost:9000/item/list");
    	WebElement objBefore = driver.findElements(By.className("test-row")).get(0);
    	String id = objBefore.findElement(By.className("test-id")).getText();
    	seleniumWait();
    	objBefore.findElement(By.className("test-edit")).click();
    	Assert.assertEquals("Is in details?", "http://localhost:9000/item/details/" + id, driver.getCurrentUrl());
    	WebElement el = driver.findElement(By.id("name"));
    	seleniumWait();
    	el.clear();
    	el.sendKeys("PoZmianie");
    	driver.findElement(By.id("test-edit")).click();
    	driver.navigate().to("http://localhost:9000/item/list");
    	seleniumWait();
    	WebElement objAfter = driver.findElements(By.className("test-row")).get(0);
    	String nameAfter = objAfter.findElement(By.className("test-name")).getText();
    	seleniumWait();
    	Assert.assertEquals("Did it change?", "PoZmianie", nameAfter);
    }
    
    @Test
    public void T003_doesItemRemoveWork() {
    	//Setup
    	itemRepo.deleteAll();
    	
    	ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
    	itemCatRepo.save(itemCat);
    	
    	Item item = new Item();
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	item = itemRepo.save(item);
    	//
    	
    	
    	driver.navigate().to("http://localhost:9000/item/list");
    	WebElement el = driver.findElements(By.className("test-row")).get(0);
    	seleniumWait();
    	el.findElement(By.className("btn-danger")).click();
    	seleniumWait();
    	driver.switchTo().alert().accept();
    	seleniumWait();
    	Assert.assertEquals("Is everything deleted?", 0, driver.findElements(By.className("test-row")).size());
    	seleniumWait();

    }
    
    @Test
    public void T004_doesItemRemoveWork_CancelRemove() {
    	//Setup
    	itemRepo.deleteAll();
    	
    	ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
    	itemCatRepo.save(itemCat);
    	
    	Item item = new Item();
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	item = itemRepo.save(item);
    	//
    	
    	
    	driver.navigate().to("http://localhost:9000/item/list");
    	WebElement el = driver.findElements(By.className("test-row")).get(0);
    	seleniumWait();
    	el.findElement(By.className("btn-danger")).click();
    	seleniumWait();
    	driver.switchTo().alert().dismiss();
    	seleniumWait();
    	Assert.assertEquals("Is everything not deleted?", 1, driver.findElements(By.className("test-row")).size());
    	seleniumWait();

    }
}