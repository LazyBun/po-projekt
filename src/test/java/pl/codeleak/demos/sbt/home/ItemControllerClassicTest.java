package pl.codeleak.demos.sbt.home;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import pl.codeleak.demos.sbt.item.Item;
import pl.codeleak.demos.sbt.item.ItemController;
import pl.codeleak.demos.sbt.item.ItemRepository;
import pl.codeleak.demos.sbt.item.ItemStatusEnum;
import pl.codeleak.demos.sbt.itemCategory.ItemCategory;
import pl.codeleak.demos.sbt.itemCategory.ItemCategoryRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
public class ItemControllerClassicTest {
	
	@Autowired
    private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Autowired
	ItemRepository itemRepo;
	
	@Autowired
	ItemController itemControl;
	
	@Autowired
	ItemCategoryRepository itemCategoryRepo;
	
	@Before
    public void initMocks(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }
	
	@After 
	public void reset_mocks() {
	    Mockito.reset(itemCategoryRepo);
	    Mockito.reset(itemRepo);
	}
	
	@Test
	public void itemListTest() throws Exception {
		//Setup
		Item item = new Item();
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);

		when(itemRepo.findAll()).thenReturn(Arrays.asList(item));
		
		//Test
		mockMvc.perform(get("/item/list"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("items", hasSize(1)));
		
		verify(itemRepo, times(1)).findAll();
		
		
	}
	
	@Test
	public void itemListTestEmpty() throws Exception {
		//Setup


		when(itemRepo.findAll()).thenReturn(Arrays.asList());
		
		//Test
		mockMvc.perform(get("/item/list"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("items", hasSize(0)));
		
		verify(itemRepo, times(1)).findAll();
		
		
	}
	
	@Test
	public void itemDetailsTest() throws Exception {
		//Setup
		ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
		
		Item item = new Item();
		long id = 1;
		item.setId(id);
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	
		when(itemRepo.findById(id)).thenReturn(item);
		when(itemCategoryRepo.findAll()).thenReturn(Arrays.asList(itemCat));
		
		//Test
		mockMvc.perform(get("/item/details/" + id))
			.andExpect(status().isOk())
			.andExpect(model().attribute("item", notNullValue()))
			.andExpect(model().attribute("categories", hasSize(1)))
			.andExpect(model().attribute("stasuses", notNullValue()));
		
		verify(itemRepo, times(1)).findById(1);
		
		
	}
	
	@Test
	public void itemDetailsTestNoCategories() throws Exception {
		//Setup
		
		Item item = new Item();
		long id = 1;
		item.setId(id);
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	
		when(itemRepo.findById(id)).thenReturn(item);
		when(itemCategoryRepo.findAll()).thenReturn(Arrays.asList());
		
		//Test
		mockMvc.perform(get("/item/details/" + id))
			.andExpect(status().isOk())
			.andExpect(model().attribute("item", notNullValue()))
			.andExpect(model().attribute("categories", hasSize(0)))
			.andExpect(model().attribute("stasuses", notNullValue()));
		
		verify(itemRepo, times(1)).findById(1);
		
		
	}
	
	@Test
	public void itemAddTest() throws Exception {
		//Setup
		ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
		
    	
		when(itemCategoryRepo.findAll()).thenReturn(Arrays.asList(itemCat));
		
		//Test
		mockMvc.perform(get("/item/add"))
			.andExpect(status().isOk())
			.andExpect(model().attribute("item", notNullValue()))
			.andExpect(model().attribute("categories", hasSize(1)))
			.andExpect(model().attribute("stasuses", notNullValue()));
		
		verify(itemCategoryRepo, times(1)).findAll();

	}
	
	@Test
	public void deleteTest() throws Exception {
		//Setup
		ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
		
		Item item = new Item();
		long id = 1;
		item.setId(id);
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	
		when(itemRepo.findById(id)).thenReturn(item);
		
		//Test
		mockMvc.perform(delete("/item/" + id))
		.andDo(print())
			.andExpect(status().is3xxRedirection());
		
		verify(itemRepo, times(1)).delete(id);
		
	}
	
	@Test
	public void deleteTestWrongId() throws Exception {
		//Setup
		long id = 1;
		when(itemRepo.findById(id)).thenReturn(null);
		//Test
		mockMvc.perform(delete("/item/" + id))
		.andDo(print())
			.andExpect(status().is3xxRedirection());
		
		verify(itemRepo, times(0)).delete(id);
		
	}
	
	
	
	
	
	
}
