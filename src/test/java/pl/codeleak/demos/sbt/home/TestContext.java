package pl.codeleak.demos.sbt.home;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.codeleak.demos.sbt.item.ItemController;
import pl.codeleak.demos.sbt.item.ItemRepository;
import pl.codeleak.demos.sbt.itemCategory.ItemCategoryRepository;
import pl.codeleak.demos.sbt.order.OrderController;
import pl.codeleak.demos.sbt.order.OrderRepository;

@Configuration
public class TestContext {
	@Bean
	ItemRepository itemRepo() {
		return Mockito.mock(ItemRepository.class);
	}
	
	@Bean
	ItemCategoryRepository itemCategoryRepo() {
		return Mockito.mock(ItemCategoryRepository.class);
	}
	
	@Bean
	OrderRepository orderRepository() {
		return Mockito.mock(OrderRepository.class);
	}
	
	@Bean
	OrderController orderController() {
		return new OrderController();
	}
	
	@Bean
	ItemController itemControl() {
		return new ItemController();
	}
}
