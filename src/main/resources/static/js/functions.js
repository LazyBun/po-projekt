function sendDeleteTo(url) {
	if (confirm('Na pewno chcesz usunąć?')) {
		$.ajax({
		    url: url,
		    type: 'DELETE',
		    success: function() {
		        window.location.reload(true);
		    }
		});
	}
}

function changeOrderState(id, url) {
	var e = document.getElementById(id);
	var state = e.options[e.selectedIndex].text;
	$.ajax({
	    url: url,
	    type: 'PUT',
	    data: state,
	    success: function() {
	        window.location.reload(true);
	    }
	});
}

function changeTracking(id, url) {
	var e = document.getElementById(id);
	var tracking = e.value;
	$.ajax({
	    url: url,
	    type: 'PUT',
	    data: tracking,
	    success: function() {
	        window.location.reload(true);
	    }
	});
}