package pl.codeleak.demos.sbt.item;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.codeleak.demos.sbt.itemCategory.ItemCategory;

/**
 * @author PC
 * Class responsible for communication with DB
 */
public interface ItemRepository extends JpaRepository<Item, Long> {
	public Item findById(long id);
	
	/**
	 * Updates item
	 * @param id
	 * @param name
	 * @param cost
	 * @param description
	 * @param status
	 * @param category
	 * @return
	 */
	@Transactional
	@Modifying
    @Query("UPDATE Item c SET "
    		+ "c.name = :name, "
    		+ "c.cost = :cost, "
    		+ "c.description = :description, "
    		+ "c.status = :status, "
    		+ "c.category = :category "
    		//+ "c.orders = :orders "
    		+ "WHERE c.id = :itemId")
    int updateItem(@Param("itemId") long companyId, 
    				@Param("name") String name,
    				@Param("cost") double cost,
    				@Param("description") String description,
    				@Param("status") ItemStatusEnum status,
    				@Param("category") ItemCategory category
    				//@Param("orders") List<OrderItem> orders
    				);
	
}
