package pl.codeleak.demos.sbt.item;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import pl.codeleak.demos.sbt.OrderItem.OrderItem;
import pl.codeleak.demos.sbt.itemCategory.ItemCategory;



/**
 * @author PC
 *	Item model
 */
@Entity
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String name;
	
	@Column
	private double cost;
	
	@Column(columnDefinition="TEXT")
	private String description; 
	
	@Column
	private ItemStatusEnum status;
	
	@ManyToOne(targetEntity=ItemCategory.class)
  	@JoinColumn
	private ItemCategory category;
	
	@OneToMany(mappedBy="item",cascade=CascadeType.ALL, orphanRemoval=true)
	private List<OrderItem> orders;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ItemStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ItemStatusEnum status) {
		this.status = status;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public List<OrderItem> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderItem> orders) {
		this.orders = orders;
	}
	
	
	
	
}
