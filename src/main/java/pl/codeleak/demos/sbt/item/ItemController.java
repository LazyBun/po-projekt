package pl.codeleak.demos.sbt.item;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.codeleak.demos.sbt.itemCategory.ItemCategoryRepository;

/**
 * @author PC
 * Controller repsonsible for Item logic.
 */
@Controller
public class ItemController {
	@Autowired
	ItemRepository itemRepo;
	
	@Autowired
	ItemCategoryRepository itemCategoryRepo;
	
	/**
	 * GET(/item/list)
	 * Lists all items in DB
	 * @param model
	 * @return page with model
	 */
	@RequestMapping("/item/list")
    String itemList(Model model) {
    	model.addAttribute("content", "fragments/item/list");
        model.addAttribute("contentFragment", "list");
        model.addAttribute("items", itemRepo.findAll());
    	return "master";
    }
	
	
	
	/**
	 * GET(/item/details/{id}).
	 * Lists data for item with {id}
	 * @param model
	 * @param id
	 * @return page with model
	 */
	@RequestMapping("/item/details/{id}")
    String itemDetails(Model model, @PathVariable long id) {
    	model.addAttribute("content", "fragments/item/details");
        model.addAttribute("contentFragment", "details");
        model.addAttribute("item", itemRepo.findById(id));
        model.addAttribute("categories", itemCategoryRepo.findAll());
        model.addAttribute("stasuses", Arrays.asList(ItemStatusEnum.values()));
    	return "master";
    }
	
	
	/**
	 * GET(/item/add).
	 * Generates page for creating new items.
	 * @param model
	 * @return page with model
	 */
	@RequestMapping("/item/add")
    String itemAdd(Model model) {
    	model.addAttribute("content", "fragments/item/add");
        model.addAttribute("contentFragment", "add");
        model.addAttribute("item", new Item());
        model.addAttribute("categories", itemCategoryRepo.findAll());
        model.addAttribute("stasuses", Arrays.asList(ItemStatusEnum.values()));
    	return "master";
    }
	
	
	/**
	 * POST(/item/edit).
	 * Updates item in DB.
	 * @param item
	 * @param bindingResult
	 * @param model
	 * @return Redirects to edited item details
	 */
	@RequestMapping(value = "/item/edit", method = RequestMethod.POST)
	public String edit(Item item,
			BindingResult bindingResult,
			final Model model) {
		
		itemRepo.updateItem(item.getId(), item.getName(), item.getCost(),
				item.getDescription(), item.getStatus(), item.getCategory());
		return "redirect:/item/details/" + item.getId();
	}
	
	
	/**
	 * POST(/item/add)
	 * @param item
	 * @param bindingResult
	 * @param model
	 * @return redirect to items list
	 */
	@RequestMapping(value = "/item/add", method = RequestMethod.POST)
	public String add(Item item,
			BindingResult bindingResult,
			final Model model) {
		
		itemRepo.save(item);
		return "redirect:/item/list";
	}
	
	
	/**
	 * DELETE(item/{id}).
	 * Removes item with {id} from DB IF it exists
	 * @param id
	 * @return redirect to items list
	 */
	@RequestMapping(value = "/item/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable long id) {
		if(itemRepo.findById(id) != null) {
			itemRepo.delete(id);
		}
		return "redirect:/item/list";
	}
	
}
