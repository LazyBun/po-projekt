package pl.codeleak.demos.sbt.item;

/**
 * @author PC
 * Item status enumeration
 */
public enum ItemStatusEnum {
	DOSTEPNY,
	NIEDOSTEPNY
}
