package pl.codeleak.demos.sbt.order;

/**
 * @author PC
 *	Order status enumeration.
 */
public enum OrderStateEnum {
	W_KOSZYKU,
	OCZEKUJACE,
	W_TRAKCIE_REALIZACJI,
	ZREALIZOWANE
}
