package pl.codeleak.demos.sbt.order;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author PC
 * Class responsible for communication with DB
 */
public interface OrderRepository extends JpaRepository<Ordered, Long> {
	public Ordered findById(long id);
	
	@Transactional
	@Modifying
    @Query("UPDATE Ordered c SET "
    		+ "c.state = :state "
    		+ "WHERE c.id = :id")
    int changeState(@Param("id") long id, @Param("state") OrderStateEnum state);
	
	@Transactional
	@Modifying
    @Query("UPDATE Ordered c SET "
    		+ "c.trackingNo = :trackingNo "
    		+ "WHERE c.id = :id")
    int changeTracking(@Param("id") long id, @Param("trackingNo") String trackingNo);
	
}
