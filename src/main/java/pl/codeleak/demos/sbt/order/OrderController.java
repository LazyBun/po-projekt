package pl.codeleak.demos.sbt.order;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.codeleak.demos.sbt.OrderItem.OrderItem;



/**
 * @author PC
 * Controller repsonsible for Order logic.
 */
@Controller
public class OrderController {
	@Autowired
	OrderRepository orderRepo;
	
	
	/**
	 * GET(/order/list)
	 * Lists all orders in DB
	 * @param model
	 * @return page with model
	 */
	@RequestMapping("/order/list")
    String orderList(Model model) {
    	model.addAttribute("content", "fragments/order/list");
        model.addAttribute("contentFragment", "list");
        model.addAttribute("orders", orderRepo.findAll());
    	return "master";
    }
    
	
	/**
	 * GET(/order/details/{id}).
	 * Lists data for order with {id}
	 * @param model
	 * @param id
	 * @return page with model
	 */
    @RequestMapping("/order/details/{id}")
    String orderDetails(Model model, @PathVariable long id) {
    	model.addAttribute("content", "fragments/order/details");
        model.addAttribute("contentFragment", "details");
        
        float sum = 0.00f;
        Ordered order = orderRepo.findById(id);
        for(OrderItem oi : order.getItems()) {
        	sum += oi.getAmount() * oi.getItem().getCost();
        }
        
        model.addAttribute("sum", sum);
        model.addAttribute("order", order);
        model.addAttribute("states", Arrays.asList(OrderStateEnum.values()));
    	return "master";
    }
	    
    /**
	 * DELETE(/order/{id}).
	 * Removes order with {id} from DB IF it exists
	 * @param id
	 * @return redirect to order list
	 */
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable long id) {
    	if(orderRepo.findById(id) != null) {
    		orderRepo.delete(id);
    	}
		return "redirect:/order/list";
	}
    
    
	/**
	 * PUT(/order/changeStatus/{id})
	 * Changes order state in DB.
	 * @param state
	 * @param id
	 * @return HTTP 200
	 */
	@RequestMapping(value = "/order/changeStatus/{id}", method = RequestMethod.PUT)
	public ResponseEntity changeState(@RequestBody String state,  @PathVariable long id) {
		
		orderRepo.changeState(id, OrderStateEnum.valueOf(state));
		return new ResponseEntity(HttpStatus.OK);
	}
	
	/**
	 * PUT(/order/changeTracking/{id})
	 * Changes order tracking No in DB.
	 * @param tracking
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/order/changeTracking/{id}", method = RequestMethod.PUT)
	public ResponseEntity changeTracking(@RequestBody String tracking,  @PathVariable long id) {
		orderRepo.changeTracking(id, tracking);
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
