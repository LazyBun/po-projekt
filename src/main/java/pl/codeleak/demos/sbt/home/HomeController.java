package pl.codeleak.demos.sbt.home;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.codeleak.demos.sbt.OrderItem.OrderItem;
import pl.codeleak.demos.sbt.OrderItem.OrderItemRepository;
import pl.codeleak.demos.sbt.item.Item;
import pl.codeleak.demos.sbt.item.ItemRepository;
import pl.codeleak.demos.sbt.item.ItemStatusEnum;
import pl.codeleak.demos.sbt.itemCategory.ItemCategory;
import pl.codeleak.demos.sbt.itemCategory.ItemCategoryRepository;
import pl.codeleak.demos.sbt.order.OrderRepository;
import pl.codeleak.demos.sbt.order.OrderStateEnum;
import pl.codeleak.demos.sbt.order.Ordered;

@Controller
class HomeController {

	
	
	@Autowired
	ItemRepository itemRepo;
	
	@Autowired
	ItemCategoryRepository itemCatRepo;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	OrderItemRepository orderItemRepo;
	
    @RequestMapping("/")
    String index(Model model) {
        return "redirect:/item/list";
    }
    
    @RequestMapping("/test")
    String test(Model model) {
        model.addAttribute("content", "fragments/hello");
        model.addAttribute("contentFragment", "hello");
        model.addAttribute("header", "fragments/empty");
        model.addAttribute("headerFragment", "empty");
        return "master";
    }
    
    @RequestMapping("/fill")
    void fill(Model model) {
    	ItemCategory itemCat = new ItemCategory();
    	itemCat.setName("Ogród");
    	itemCatRepo.save(itemCat);
    	
    	ItemCategory itemCat2 = new ItemCategory();
    	itemCat2.setName("Dom");
    	itemCatRepo.save(itemCat2);
    	
    	Item item = new Item();
    	item.setName("Łopata");    
    	item.setStatus(ItemStatusEnum.DOSTEPNY);
    	item.setDescription("Z najlepszej stali na rynku!");
    	item.setCost(119.99);
    	item.setCategory(itemCat);
    	item = itemRepo.save(item);
    	
    	
    	
    	Item item2 = new Item();
    	item2.setName("Węgiel");    
    	item2.setStatus(ItemStatusEnum.NIEDOSTEPNY);
    	item2.setDescription("Najlepszy węgiel w mieście!");
    	item2.setCost(10.00);
    	item2.setCategory(itemCat2);
    	item2 = itemRepo.save(item2);
    	
    	
    	Ordered order = new Ordered();
    	order.setWorkerName("Henryk Lenryk");
    	order.setState(OrderStateEnum.OCZEKUJACE);
    	LocalDateTime now = LocalDateTime.now();
    	Date d2 = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
    	order.setDate(d2);
    	order = orderRepo.save(order);
    	
    	OrderItem oItem1 = new OrderItem();
    	oItem1.setAmount(3);
    	oItem1.setItem(item);
    	oItem1.setOrdered(order);
    	orderItemRepo.save(oItem1);
    	
    }
    
    
}
