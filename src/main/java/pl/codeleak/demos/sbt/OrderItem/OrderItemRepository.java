package pl.codeleak.demos.sbt.OrderItem;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author PC
 * Class responsible for communication with DB
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
	public OrderItem findById(long id);
	
	
	
	/**
	 * Updates item amount in OrderItem
	 * @param id
	 * @param amount
	 * @return
	 */
	@Transactional
	@Modifying
    @Query("UPDATE OrderItem c SET "
    		+ "c.amount = :amount "
    		+ "WHERE c.id = :id")
    int updateItem(@Param("id") long id, @Param("amount") int amount);
	
}
