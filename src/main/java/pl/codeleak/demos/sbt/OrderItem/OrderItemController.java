package pl.codeleak.demos.sbt.OrderItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.codeleak.demos.sbt.item.ItemRepository;
import pl.codeleak.demos.sbt.order.OrderRepository;

/**
 * @author PC
 * Controller repsonsible for OrderItem logic.
 */
@Controller
public class OrderItemController {
	
	@Autowired
	OrderItemRepository orderItemRepo;
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	ItemRepository itemRepo;
	
	/**
	 * GET(/orderitem/details/{id})
	 * Lists data for OrderItem with {id}
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/orderitem/details/{id}", method = RequestMethod.GET)
	public String details(Model model, @PathVariable long id) {
		model.addAttribute("content", "fragments/orderitem/details");
        model.addAttribute("contentFragment", "details");
		model.addAttribute("oItem", orderItemRepo.findById(id));
		return "master";
	}
	
	
	/**
	 * GET(/item/add).
	 * Generates page for creating new OrderItems.
	 * @param model
	 * @param Order id
	 * @return page with model
	 */
	@RequestMapping(value = "/orderitem/add/{id}", method = RequestMethod.GET)
	public String addToOrder(Model model, @PathVariable long id) {
		model.addAttribute("content", "fragments/orderitem/add");
        model.addAttribute("contentFragment", "add");
        OrderItem oi = new OrderItem();
        oi.setOrdered(orderRepo.findById(id));
		model.addAttribute("oItem", oi);
		model.addAttribute("items", itemRepo.findAll());
		return "master";
	}
	
	
	/**
	 * POST(/orderitem/add)
	 * Adds OrderItem to DB.
	 * @param item
	 * @param bindingResult
	 * @param model
	 * @return redirects to order details
	 */
	@RequestMapping(value = "/orderitem/add", method = RequestMethod.POST)
	public String add(OrderItem item,
			BindingResult bindingResult,
			final Model model) {
		
		orderItemRepo.save(item);
		return "redirect:/order/details/" + item.getOrdered().getId();
	}
	
	
	/**
	 * DELETE(/orderitem/{id}).
	 * Removes item with {id} from DB IF it exists
	 * @param id
	 * @return redirect to order details
	 */
	@RequestMapping(value = "/orderitem/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable long id) {
		if(orderItemRepo.findById(id) != null) {
			orderItemRepo.delete(id);
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	
	/**
	 * POSt(/orderitem/edit)
	 * Updates item amount for OrderItem in DB.1
	 * @param item
	 * @param bindingResult
	 * @param model
	 * @return redirect to order details
	 */
	@RequestMapping(value = "/orderitem/edit", method = RequestMethod.POST)
	public String edit(OrderItem item,
			BindingResult bindingResult,
			final Model model) {
		orderItemRepo.updateItem(item.getId(), item.getAmount());
		
		return "redirect:/order/details/" + orderItemRepo.findById(item.getId()).getOrdered().getId();
	}
	
	
	
}
