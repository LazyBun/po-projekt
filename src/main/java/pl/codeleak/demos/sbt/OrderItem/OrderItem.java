package pl.codeleak.demos.sbt.OrderItem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import pl.codeleak.demos.sbt.item.Item;
import pl.codeleak.demos.sbt.order.Ordered;

/**
 * @author PC
 * OrderItem model.
 * OrderItem is connection between Item and Order.
 */
@Entity
public class OrderItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne(targetEntity=Item.class)
  	@JoinColumn
	private Item item;
	
	@ManyToOne(targetEntity=Ordered.class)
  	@JoinColumn
	private Ordered ordered;
	
	@Column
	private int amount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}


	public Ordered getOrdered() {
		return ordered;
	}

	public void setOrdered(Ordered ordered) {
		this.ordered = ordered;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}
