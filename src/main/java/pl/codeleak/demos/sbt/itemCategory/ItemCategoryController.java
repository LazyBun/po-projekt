package pl.codeleak.demos.sbt.itemCategory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author PC
 * Controller repsonsible for ItemCategory logic.
 */
@Controller
public class ItemCategoryController {
	@Autowired
	ItemCategoryRepository itemCategoryRepo;
	
	@RequestMapping(value = "/api/itemcategory/", method = RequestMethod.GET)
	public List<ItemCategory> getAll() {
		return itemCategoryRepo.findAll();
	}
	
	@RequestMapping(value = "/api/itemcategory/{id}", method = RequestMethod.GET)
	public ItemCategory getOne(@PathVariable long id) {
		return itemCategoryRepo.findById(id);
	}
	
	@RequestMapping(value = "/api/itemcategory/", method = RequestMethod.POST)
	public ItemCategory add(@RequestBody ItemCategory order) {
		return itemCategoryRepo.save(order);
	}
}
