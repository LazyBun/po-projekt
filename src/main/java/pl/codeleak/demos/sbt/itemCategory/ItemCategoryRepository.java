package pl.codeleak.demos.sbt.itemCategory;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author PC
 * Class responsible for communication with DB
 */
public interface ItemCategoryRepository extends JpaRepository<ItemCategory, Long> {
	public ItemCategory findById(long id);
}
